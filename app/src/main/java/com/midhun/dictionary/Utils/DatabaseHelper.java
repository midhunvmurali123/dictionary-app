package com.midhun.dictionary.Utils;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DatabaseHelper extends SQLiteOpenHelper {

    private String DB_PATH = null;
    private static String DB_NAME = "eng_dictionary.db";
    private SQLiteDatabase sqLiteDatabase;
    private final Context context;


    public DatabaseHelper(Context context) {
        super(context,DB_NAME,null,1);
        this.context = context;
        this.DB_PATH = "/data/data/" + context.getPackageName() + "/" + "databases/";
        Log.e("Path 1", DB_PATH);
    }

    public void createDataBase() throws IOException{
        boolean dbExist = checkDataBase();
        if (!dbExist){
            this.getReadableDatabase();
            try {
                copyDataBase();
            }
            catch (IOException e){
                throw new Error("Error Copying Database");
            }
        }
    }

    public boolean checkDataBase(){
        SQLiteDatabase checkDB = null;
        try {
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath,null, SQLiteDatabase.OPEN_READONLY);
        }
        catch (SQLException e){

        }
        if (checkDB != null){
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }

    private void copyDataBase() throws IOException{

        InputStream inputStream = context.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream outputStream = new FileOutputStream(outFileName);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) > 0){
            outputStream.write(buffer, 0 ,length);
        }
        outputStream.flush();
        outputStream.close();
        inputStream.close();
        Log.i("CopyDataBase","DataBase Copied");
    }

    public void openDataBase() throws SQLException{
        String dbPath = DB_PATH + DB_NAME;
        sqLiteDatabase = SQLiteDatabase.openDatabase(dbPath,null,SQLiteDatabase.OPEN_READWRITE);
    }

    @Override
    public synchronized void close() {
        if (sqLiteDatabase != null)
            sqLiteDatabase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            this.getReadableDatabase();
            context.deleteDatabase(DB_NAME);
            copyDataBase();
        }
        catch (IOException e){
            e.printStackTrace();

        }
    }

    public Cursor getMeaning(String text){
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT en_definition,example,synonyms,antonyms FROM words WHERE en_word==UPPER('"+text+"')",null);
        return cursor;
    }

    public Cursor getSuggestions(String text){
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT _id, en_word FROM words WHERE en_word LIKE '"+text+"%' LIMIT 40",null);
        return cursor;
    }

    public void insertHistory(String text){
        sqLiteDatabase.execSQL("INSERT INTO history(word) VALUES(UPPER('"+text+"'))");
    }

    public Cursor getHistory(){
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT DISTINCT word, en_definition FROM history h JOIN words w ON h.word==w.en_word ORDER BY h._id DESC", null);
        return cursor;
    }

    public void deleteHistory(){
        sqLiteDatabase.execSQL("DELETE FROM history");
    }
}
