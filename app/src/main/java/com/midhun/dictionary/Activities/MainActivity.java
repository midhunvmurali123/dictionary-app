package com.midhun.dictionary.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toolbar;

import com.midhun.dictionary.Adapters.HistoryAdapter;
import com.midhun.dictionary.Models.History;
import com.midhun.dictionary.R;
import com.midhun.dictionary.Utils.DatabaseHelper;
import com.midhun.dictionary.Utils.LoadDataBaseAsync;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    SearchView searchView;
    static DatabaseHelper databaseHelper;
    static boolean databaseopened = false;

    SimpleCursorAdapter suggestionAdapter;

    ArrayList<History> histories;
    RecyclerView recyclerView;
    HistoryAdapter historyAdapter;
    RecyclerView.LayoutManager layoutManager;

    RelativeLayout emptyHistory;
    Cursor cursorHistory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        emptyHistory = (RelativeLayout) findViewById(R.id.empty_history);
        recyclerView = (RecyclerView) findViewById(R.id.rv_history);
        layoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(layoutManager);

        fetchHistory();
        
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.inflateMenu(R.menu.main_menu);
        searchView = (SearchView) findViewById(R.id.search_view);
        searchView.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        setSupportActionBar(toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                if(item.getItemId()==R.id.action_settings)
                {
                    Intent intent = new Intent(MainActivity.this,SettingsActivity.class);
                    startActivity(intent);
                    finish();
                }
                else if(item.getItemId()== R.id.action_exit)
                {
                    System.exit(0);
                    return true;
                }
                else{
                    // do something
                }

                return false;
            }
        });

        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);

            }

        });

        databaseHelper = new DatabaseHelper(this);
        if (databaseHelper.checkDataBase()){
            openDatabase();
        }
        else{
            LoadDataBaseAsync loadDataBaseAsync = new LoadDataBaseAsync(MainActivity.this);
            loadDataBaseAsync.execute();
        }

        //set up simpleCursorAdapter

        final String[] from = new String[] {"en_word"};
        final int[] to = new int[] {R.id.suggestion_text};

        suggestionAdapter = new SimpleCursorAdapter(MainActivity.this, R.layout.suggestion_row, null, from, to,0){

            @Override
            public void changeCursor(Cursor cursor) {
                super.changeCursor(cursor);
            }
        };
        searchView.setSuggestionsAdapter(suggestionAdapter);
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                return true;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                // add click to the search box
                CursorAdapter cursorAdapter = searchView.getSuggestionsAdapter();
                Cursor cursor = cursorAdapter.getCursor();
                cursor.moveToPosition(position);
                String Clicked_word = cursor.getString(cursor.getColumnIndex("en_word"));
                searchView.setQuery(Clicked_word,false);
                searchView.clearFocus();
                searchView.setFocusable(false);

                Intent intent = new Intent(MainActivity.this,WordMeaningActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("en_word",Clicked_word);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                String text = searchView.getQuery().toString();
                Pattern pattern = Pattern.compile("[A-Za-z \\-.]{1,25}");
                Matcher matcher =pattern.matcher(text);


                if (matcher.matches()){
                    Cursor cursor = databaseHelper.getMeaning(text);
                    if (cursor.getCount() == 0){

                        showAlertDialog();

                    }
                    else {
                        searchView.clearFocus();
                        searchView.setFocusable(false);

                        Intent intent = new Intent(MainActivity.this,WordMeaningActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("en_word",text);
                        intent.putExtras(bundle);
                        startActivity(intent);
                        finish();

                    }
                }
                else {
                    showAlertDialog();
                }


                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchView.setIconifiedByDefault(false);
                Pattern pattern = Pattern.compile("[A-Za-z \\-.]{1,25}");
                Matcher matcher =pattern.matcher(newText);

                if (matcher.matches()){
                    Cursor cursorSuggestions = databaseHelper.getSuggestions(newText);
                    suggestionAdapter.changeCursor(cursorSuggestions);
                }

                return false;
            }
        });


    }

    private void showAlertDialog() {
        searchView.setQuery("",false);
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.MyDialogTheme);
        builder.setTitle("Word not found");
        builder.setMessage("Please search again");

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                searchView.clearFocus();
            }
        });
        AlertDialog dialog =builder.create();
        dialog.show();
    }

    private void fetchHistory() {

        histories = new ArrayList<>();
        historyAdapter = new HistoryAdapter(histories, MainActivity.this);
        recyclerView.setAdapter(historyAdapter);

        History history;
        if (databaseopened){
            cursorHistory = databaseHelper.getHistory();
            if (cursorHistory.moveToFirst()){
                do {
                    history = new History(cursorHistory.getString(cursorHistory.getColumnIndex("word")),cursorHistory.getString(cursorHistory.getColumnIndex("en_definition")));
                    histories.add(history);
                }
                while (cursorHistory.moveToNext());
            }
            historyAdapter.notifyDataSetChanged();

            if (historyAdapter.getItemCount() == 0){
                emptyHistory.setVisibility(View.VISIBLE);
            }
            else {
                emptyHistory.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        fetchHistory();
    }

    public static void openDatabase(){
        try {
            databaseHelper.openDataBase();
            databaseopened = true;
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }

    private void setSupportActionBar(Toolbar toolbar) {
    }

}
