package com.midhun.dictionary.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;


import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.Toolbar;


import com.google.android.material.tabs.TabLayout;
import com.midhun.dictionary.Fragments.FragmentAntonyms;
import com.midhun.dictionary.Fragments.FragmentDefinition;
import com.midhun.dictionary.Fragments.FragmentExample;
import com.midhun.dictionary.Fragments.FragmentSynonyms;
import com.midhun.dictionary.R;
import com.midhun.dictionary.Utils.DatabaseHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordMeaningActivity extends AppCompatActivity {
    private ViewPager viewPager;

    String enWord;
    DatabaseHelper databaseHelper;
    Cursor cursor = null;

    public String enDefinition;
    public String example;
    public String synonyms;
    public String antonyms;
    TextToSpeech textToSpeech;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_word_meaning);

        Bundle bundle = getIntent().getExtras();
        enWord = bundle.getString("en_word");

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null){
            if ("text/plain".equals(type)){
                String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);

                if (sharedText != null){

                    Pattern pattern = Pattern.compile("[A-Za-z \\-.]{1,25}");
                    Matcher matcher =pattern.matcher(sharedText);

                    if (matcher.matches()){
                        enWord = sharedText;
                    }
                    else {
                        enWord = "Not Available";
                    }

                }
            }
        }

        databaseHelper = new DatabaseHelper(this);
        try{
            databaseHelper.openDataBase();
        }
        catch (SQLException e){
            throw e;
        }
        cursor = databaseHelper.getMeaning(enWord);
        if (cursor.moveToFirst()){

            enDefinition = cursor.getString(cursor.getColumnIndex("en_definition"));
            example = cursor.getString(cursor.getColumnIndex("example"));
            synonyms = cursor.getString(cursor.getColumnIndex("synonyms"));
            antonyms = cursor.getString(cursor.getColumnIndex("antonyms"));
            databaseHelper.insertHistory(enWord);

        }
        else {
            enWord = "Not Available";
        }

        ImageButton btnSpeek = (ImageButton)findViewById(R.id.btn_speek);
        btnSpeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textToSpeech = new TextToSpeech(WordMeaningActivity.this, new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if (status == TextToSpeech.SUCCESS){
                            int result = textToSpeech.setLanguage(Locale.getDefault());
                            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED){
                                Log.e("ERROR","This Language is not Supported");
                            }
                            else {
                                textToSpeech.speak(enWord,TextToSpeech.QUEUE_FLUSH,null);
                            }
                        }
                        else {
                            Log.e("ERROR","Initialization Failed");
                        }

                    }
                });
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.mtoolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(enWord);
        toolbar.setNavigationIcon(R.drawable.ic_navigate_before_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WordMeaningActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        viewPager = (ViewPager) findViewById(R.id.tab_viewpager);
        if (viewPager != null){
            setupViewPager(viewPager);
        }


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setSupportActionBar(Toolbar toolbar) {
    }


    private class ViewPagerAdapter extends FragmentPagerAdapter{
        private final List<Fragment> fragmentList = new ArrayList<>();
        private final List<String> mfragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(@NonNull FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public void addFrags(Fragment fragment, String title){
            fragmentList.add(fragment);
            mfragmentTitleList.add(title);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }


        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return mfragmentTitleList.get(position);
        }
    }

    public void setupViewPager(ViewPager viewPager){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrags(new FragmentDefinition(), "Definition");
        adapter.addFrags(new FragmentSynonyms(), "Synonyms");
        adapter.addFrags(new FragmentAntonyms(), "Antonyms");
        adapter.addFrags(new FragmentExample(), "Example");
        viewPager.setAdapter(adapter);
    }

}
