package com.midhun.dictionary.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.midhun.dictionary.Activities.WordMeaningActivity;
import com.midhun.dictionary.R;

public class FragmentDefinition extends Fragment {
    public FragmentDefinition() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragments_definition, container, false);

        Context context = getActivity();
        TextView text = view.findViewById(R.id.text_viewD);

        String en_definition =((WordMeaningActivity)context).enDefinition;
        text.setText(en_definition);
        if (en_definition == null){
            text.setText("No Definition Found");
        }
        return view;
    }
}
