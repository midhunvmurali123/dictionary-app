package com.midhun.dictionary.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.midhun.dictionary.Activities.WordMeaningActivity;
import com.midhun.dictionary.R;

public class FragmentAntonyms extends Fragment {
    public FragmentAntonyms() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragments_definition, container, false);

        Context context = getActivity();
        TextView text = view.findViewById(R.id.text_viewD);

        String antonyms =((WordMeaningActivity)context).antonyms;
        if (antonyms != null){
            antonyms = antonyms.replaceAll(",",",\n");
            text.setText(antonyms);
        }
        if (antonyms == null){
            text.setText("No Antonyms Found");
        }
        return view;
    }
}
